const express = require('express')
const app = express()
const port = 3005

// app.set('view engine', 'html');
// app.engine('html', require('ejs').renderFile);
// app.set('view engine', 'html');
app.get('/', (req, res) => {
  res.sendfile('hello.html')
})

app.post('/', (req, res) => { 
  res.send('Got a POST request')
})
app.put('/user', (req, res) => {
  res.send('Got a PUT request at /user')
})
app.delete('/user', (req, res) => {
  res.send('Got a DELETE request at /user')
})
// app.use(express.static(__dirname))
app.use('/static', express.static(__dirname))

app.listen(port, () => {
  console.log(`🚀 Server ready at http://localhost:${port}`);
})