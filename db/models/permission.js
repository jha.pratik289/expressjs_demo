const Sequelize = require('sequelize')
const sequelize = require('../../utils/database')

const Permission = sequelize.define('permission',{
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    name: {
        type: Sequelize.TEXT
    },
    created_at: {
        allowNull: false,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
        type: Sequelize.DATE
    },
    updated_at: {
        allowNull: false,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
        type: Sequelize.DATE
    },
    deleted_at: {
        allowNull: true,
        type: Sequelize.DATE
    }
},{
    timestamps: false,
}
)

module.exports = Permission