// Import the sequelize object on which
// we have defined model.
const sequelize = require('./utils/database')

// Import the user model we have defined
const User = require('./db/models/user')
const Userdemo = require('./db/models/userdemo')
const Permission = require('./db/models/permission')
const {Op} = require("sequelize");

// Create all the table defined using
// sequelize in Database

// Sync all models that are not
// already in the database
sequelize.sync()

// Add the User in the table

//  User.create({name: 'two1',email:'p1@gmail.com'});
//  User.create({ name: 'two',email:'p1@gmail.com' }).then(two => {
//     console.log("Jane's auto-generated ID:", two);
//   });

// search for known ids
// User.findByPk(1).then(user => {
// user will be an instance of user and stores the content of the table entry
// with id 1. if such an entry is not defined you will get null
// console.warn('findByPk value: ', user);
// })

// search for attributes
// User.findOne({ where: { name: 'two1' } }).then(user => {
// user will be the first entry of the users table with the name 'two1' || null
// console.warn('findOne value: ', user);
// })

// search for attributes with specific column/feild/attributes
// User.findOne({
  // where: { name: 'two1' },
  // attributes: ['user_id', 'email', 'name']
// }).then(user => {
  // user will be the first entry of the users table with the name 'two1' || null
  // console.warn('findOne value: ', user);
// })

User
  .findAndCountAll({
     where: {
        name: {
          [Op.like]: 'two1%'
        }
     },
     offset: 10,
     limit: 2
  })
  .then(result => {
    console.log(result.count);
    console.log(result.rows);
  });
// Force sync all models
// It will drop the table first
// and re-create it afterwards
// sequelize.sync({force:true})
